package com.example.translator.rest;

import com.example.translator.translator.RankingEntry;
import com.example.translator.translator.TranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class TranslatorController {

    private TranslationService translationService;

    @Autowired
    public TranslatorController(TranslationService translationService) {
        this.translationService = translationService;
    }

    @GetMapping("ranking")
    public List<RankingEntry> getRanking() {
        return translationService.getRanking();
    }

    @PostMapping("translate")
    public String translate(@RequestParam boolean quoted, String text) {
        if(quoted) {
            return translationService.translateWithQuotes(text);
        } else {
            return translationService.translate(text);
        }
    }
}
