package com.example.translator.exception;

public class DictionaryMalformed extends RuntimeException {

    public DictionaryMalformed(String message) {
        super(message);
    }
}
