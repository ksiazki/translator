package com.example.translator.dictionary;

import java.util.Map;

public interface Dictionary {

    Map<String, String> getDictionary();
}
