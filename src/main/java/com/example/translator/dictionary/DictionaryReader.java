package com.example.translator.dictionary;

import com.example.translator.exception.DictionaryMalformed;
import com.example.translator.exception.DictionaryNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Stream;

public class DictionaryReader {

    private DictionaryReader() {
        throw new IllegalStateException("Util class.");
    }

    public static Map<String, String> readDictionary() {
        String fileContent = readFile();
        return convertToMap(fileContent);
    }

    private static String readFile()
    {
        StringBuilder contentBuilder = new StringBuilder();

        Path filePath = getDictionaryPath();

        try (Stream<String> stream = Files.lines( filePath, StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return contentBuilder.toString();
    }

    private static Path getDictionaryPath() {
        URL res = DictionaryReader.class.getClassLoader().getResource("static\\dictionary.json");
        if(res == null) {
            throw new DictionaryNotFoundException("Cannot find dictionary file");
        }
        try {
            return Paths.get(res.toURI());
        } catch (URISyntaxException e) {
            throw new DictionaryNotFoundException("Cannot find dictionary file", e);
        }
    }

    private static Map<String, String> convertToMap(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {

            return mapper.readValue(json, Map.class);

        } catch (IOException e) {
            throw new DictionaryMalformed("Dictionary is malformed. " + e.getMessage());
        }
    }
}
