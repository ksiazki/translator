package com.example.translator.dictionary.impl;

import com.example.translator.dictionary.Dictionary;
import com.example.translator.dictionary.DictionaryReader;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DictionaryImpl implements Dictionary {

    private final Map<String, String> dictionary;

    public DictionaryImpl() {
        this.dictionary = DictionaryReader.readDictionary();
    }

    @Override
    public Map<String, String> getDictionary() {
        return dictionary;
    }
}
