package com.example.translator.translator;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RankingEntry {

    private String word;
    private Integer count;

    public void increaseCount() {
        count++;
    }
}
