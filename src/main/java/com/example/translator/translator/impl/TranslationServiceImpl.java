package com.example.translator.translator.impl;

import com.example.translator.dictionary.Dictionary;
import com.example.translator.dictionary.DictionaryReader;
import com.example.translator.translator.RankingEntry;
import com.example.translator.translator.TranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TranslationServiceImpl implements TranslationService {

    private final Map<String, String> dictionary;
    private final List<RankingEntry> ranking;

    @Autowired
    public TranslationServiceImpl(Dictionary dictionary) {
        this.dictionary = dictionary.getDictionary();
        this.ranking = initRanking();
    }

    @Override
    public String translate(String text) {
        String trim = text.trim();
        String[] words = trim.split(" ");
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < words.length; i++) {
            if(i > 0) {
                sb.append(" ");
            }
            String translation = dictionary.get(words[i]);
            if(translation != null) {
                sb.append(translation);
                increaseRanking(words[i]);
            } else {
                sb.append(words[i]);
            }
        }
        return sb.toString();
    }

    @Override
    public String translateWithQuotes(String text) {
        String trim = text.trim();
        String[] words = trim.split(" ");
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < words.length; i++) {
            if(i > 0) {
                sb.append(" ");
            }
            sb.append("\"");
            String translation = dictionary.get(words[i]);
            if(translation != null) {
                sb.append(translation);
                increaseRanking(words[i]);
            } else {
                sb.append(words[i]);
            }
            sb.append("\"");
        }
        return sb.toString();
    }

    private void increaseRanking(String word) {
        ranking.stream()
                .filter(r -> r.getWord().equals(word))
                .findFirst().ifPresent(RankingEntry::increaseCount);
    }

    @Override
    public List<RankingEntry> getRanking() {
        sortRanking();
        return ranking;
    }

    private void sortRanking() {
        ranking.sort(Comparator.comparing(RankingEntry::getCount).reversed());
    }

    private List<RankingEntry> initRanking() {
        List<RankingEntry> ranking = new ArrayList<>(dictionary.size());
        dictionary.entrySet().stream()
                .forEach(d -> ranking.add(new RankingEntry(d.getKey(), 0)));
        return ranking;
    }
}
