package com.example.translator.translator;

import java.util.List;

public interface TranslationService {

    String translate(String text);

    String translateWithQuotes(String text);

    List<RankingEntry> getRanking();
}
