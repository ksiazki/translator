package com.example.translator.translator.impl;

import com.example.translator.dictionary.Dictionary;
import com.example.translator.testutils.DummyDictionary;
import com.example.translator.testutils.DummyDictionaryNotEmpty;
import com.example.translator.translator.RankingEntry;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

class TranslationServiceImplTest {

    private Dictionary dictionary = new DummyDictionary();

    @Test
    public void shouldGetRanking() {
        // given
        DummyDictionaryNotEmpty dummyDictionary = new DummyDictionaryNotEmpty();
        TranslationServiceImpl translationService = new TranslationServiceImpl(dummyDictionary);

        // when
        List<RankingEntry> ranking = translationService.getRanking();

        // then
        assertThat(ranking, notNullValue());
        assertThat(ranking.size(), equalTo(dummyDictionary.getDictionarySize()));
        ranking.forEach(r -> assertThat(r.getCount(), equalTo(0)));
    }

    @Test
    public void shouldReturnCorrectRanking() throws IllegalAccessException {
        // given
        TranslationServiceImpl translationService = new TranslationServiceImpl(dictionary);
        List<RankingEntry> initialRanking = new ArrayList<>();
        RankingEntry re1 = new RankingEntry("aa", 0);
        RankingEntry re2 = new RankingEntry("bb", 10);
        RankingEntry re3 = new RankingEntry("cc", 6);
        initialRanking.add(re1);
        initialRanking.add(re2);
        initialRanking.add(re3);
        FieldUtils.writeField(translationService, "ranking", initialRanking, true);

        // when
        List<RankingEntry> ranking = translationService.getRanking();

        // then
        assertThat(ranking, notNullValue());
        assertThat(ranking.get(0), equalTo(re2));
        assertThat(ranking.get(1), equalTo(re3));
        assertThat(ranking.get(2), equalTo(re1));
    }

    @Test
    public void shouldReturnCorrectRankingAfterTranslation() throws IllegalAccessException {
        // given
        TranslationServiceImpl translationService = new TranslationServiceImpl(dictionary);
        List<RankingEntry> initialRanking = new ArrayList<>();
        RankingEntry re1 = new RankingEntry("key1", 0);
        RankingEntry re2 = new RankingEntry("key2", 5);
        RankingEntry re3 = new RankingEntry("key3", 4);
        initialRanking.add(re1);
        initialRanking.add(re2);
        initialRanking.add(re3);
        FieldUtils.writeField(translationService, "ranking", initialRanking, true);
        Map<String, String> dictionary = Map.of("key1", "val1", "key2", "val2", "key3", "val3");
        FieldUtils.writeField(translationService, "dictionary", dictionary, true);

        // when
        translationService.translate("key3 key3 key3 key3 key2 key1");
        List<RankingEntry> ranking = translationService.getRanking();

        // then
        assertThat(ranking, notNullValue());
        RankingEntry rank1 = ranking.get(0);
        assertThat(rank1.getWord(), equalTo("key3"));
        assertThat(rank1.getCount(), equalTo(8));
        RankingEntry rank2 = ranking.get(1);
        assertThat(rank2.getWord(), equalTo("key2"));
        assertThat(rank2.getCount(), equalTo(6));
        RankingEntry rank3 = ranking.get(2);
        assertThat(rank3.getWord(), equalTo("key1"));
        assertThat(rank3.getCount(), equalTo(1));
    }

    @Test
    public void shouldReturnCorrectRankingAfterTranslationWithQuotes() throws IllegalAccessException {
        // given
        TranslationServiceImpl translationService = new TranslationServiceImpl(dictionary);
        List<RankingEntry> initialRanking = new ArrayList<>();
        RankingEntry re1 = new RankingEntry("key1", 0);
        RankingEntry re2 = new RankingEntry("key2", 5);
        RankingEntry re3 = new RankingEntry("key3", 4);
        initialRanking.add(re1);
        initialRanking.add(re2);
        initialRanking.add(re3);
        FieldUtils.writeField(translationService, "ranking", initialRanking, true);
        Map<String, String> dictionary = Map.of("key1", "val1", "key2", "val2", "key3", "val3");
        FieldUtils.writeField(translationService, "dictionary", dictionary, true);

        // when
        translationService.translateWithQuotes("key3 key3 key3 key3 key2 key1");
        List<RankingEntry> ranking = translationService.getRanking();

        // then
        assertThat(ranking, notNullValue());
        RankingEntry rank1 = ranking.get(0);
        assertThat(rank1.getWord(), equalTo("key3"));
        assertThat(rank1.getCount(), equalTo(8));
        RankingEntry rank2 = ranking.get(1);
        assertThat(rank2.getWord(), equalTo("key2"));
        assertThat(rank2.getCount(), equalTo(6));
        RankingEntry rank3 = ranking.get(2);
        assertThat(rank3.getWord(), equalTo("key1"));
        assertThat(rank3.getCount(), equalTo(1));
    }

    @Test
    public void shouldTranslateCorrectly() throws IllegalAccessException {
        // given
        TranslationServiceImpl translationService = new TranslationServiceImpl(dictionary);
        Map<String, String> dictionary = Map.of("key1", "val1", "key2", "val2", "key3", "val3");
        FieldUtils.writeField(translationService, "dictionary", dictionary, true);
        String expected = "val2 val1 val1 val3";

        // when
        String translation = translationService.translate("key2 key1 key1 key3");

        // then
        assertThat(translation, equalTo(expected));
    }

    @Test
    public void shouldTranslateCorrectlyWithWordsNotIinDictionary() throws IllegalAccessException {
        // given
        TranslationServiceImpl translationService = new TranslationServiceImpl(dictionary);
        Map<String, String> dictionary = Map.of("key1", "val1", "key2", "val2", "key3", "val3");
        FieldUtils.writeField(translationService, "dictionary", dictionary, true);
        String expected = "val2 val1 abc val1 val3";

        // when
        String translation = translationService.translate("key2 key1 abc key1 key3");

        // then
        assertThat(translation, equalTo(expected));
    }

    @Test
    public void shouldTranslateWithQuotesCorrectly() throws IllegalAccessException {
        // given
        TranslationServiceImpl translationService = new TranslationServiceImpl(dictionary);
        Map<String, String> dictionary = Map.of("key1", "val1", "key2", "val2", "key3", "val3");
        FieldUtils.writeField(translationService, "dictionary", dictionary, true);
        String expected = "\"val2\" \"val1\" \"val1\" \"val3\"";

        // when
        String translation = translationService.translateWithQuotes("key2 key1 key1 key3");

        // then
        assertThat(translation, equalTo(expected));
    }

    @Test
    public void shouldTranslateWithQuotesCorrectlyWithWordsNotIinDictionary() throws IllegalAccessException {
        // given
        TranslationServiceImpl translationService = new TranslationServiceImpl(dictionary);
        Map<String, String> dictionary = Map.of("key1", "val1", "key2", "val2", "key3", "val3");
        FieldUtils.writeField(translationService, "dictionary", dictionary, true);
        String expected = "\"val2\" \"val1\" \"abc\" \"val1\" \"val3\"";

        // when
        String translation = translationService.translateWithQuotes("key2 key1 abc key1 key3");

        // then
        assertThat(translation, equalTo(expected));
    }
}