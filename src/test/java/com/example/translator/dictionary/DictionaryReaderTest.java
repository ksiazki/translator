package com.example.translator.dictionary;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

class DictionaryReaderTest {

    @Test
    public void shouldReadDictionary() {
        // given
        Map<String, String> expected = new HashMap<>();
        expected.put("Ala", "Alice");
        expected.put("ma", "has");
        expected.put("kota", "a cat");
        expected.put("jesteś", "you are");
        expected.put("sterem", "the helm");
        expected.put("białym", "white");
        expected.put("żołnierzem", "soldier");
        expected.put("nosisz", "wear");
        expected.put("spodnie", "trousers");
        expected.put("więc", "so");
        expected.put("walcz", "fight");

        // when
        Map<String, String> result = DictionaryReader.readDictionary();

        // then
        assertThat(result, notNullValue());
        expected.entrySet().stream()
                .forEach(e -> assertThat(result.get(e.getKey()), equalTo(e.getValue())));
    }
}