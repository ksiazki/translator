package com.example.translator.testutils;

import com.example.translator.dictionary.Dictionary;

import java.util.HashMap;
import java.util.Map;

public class DummyDictionary implements Dictionary {

    private final Map<String, String> dictionary = new HashMap<>();

    @Override
    public Map<String, String> getDictionary() {
        return dictionary;
    }
}
