package com.example.translator.testutils;

import com.example.translator.dictionary.Dictionary;

import java.util.HashMap;
import java.util.Map;

public class DummyDictionaryNotEmpty implements Dictionary {

    private final Map<String, String> dictionary;

    public DummyDictionaryNotEmpty() {
        this.dictionary = Map.of("key1", "val1", "key2", "val2");
    }

    @Override
    public Map<String, String> getDictionary() {
        return dictionary;
    }

    public int getDictionarySize() {
        return dictionary.size();
    }
}
